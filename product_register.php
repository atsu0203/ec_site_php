<?php
session_start();
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>商品登録</title>
  </head>

  <body>
    <h1>　ECサイト</h1>
    <h2>　商品登録</h2>
    <span class="error"><?php if(isset($_SESSION["ProductErr"]["Name"])){echo $_SESSION["ProductErr"]["Name"]; echo"<br>";} ?>
                        <?php if(isset($_SESSION["ProductErr"]["Image"])){echo $_SESSION["ProductErr"]["Image"]; echo"<br>";} ?>
                        <?php if(isset($_SESSION["ProductErr"]["Introduction"])){echo $_SESSION["ProductErr"]["Introduction"]; echo"<br>";} ?>
                        <?php if(isset($_SESSION["ProductErr"]["Price"])){echo $_SESSION["ProductErr"]["Price"]; echo"<br>";} ?></span>
      <form action='product_confirm.php'  method='POST' enctype="multipart/form-data">
      <p>　商品名・商品画像・紹介文・価格を入力してください</p>
      <p><label>　　商品名　</label><input type="text" name="Name" value="<?php if(!empty($_SESSION['Name'])){ echo $_SESSION['Name']; } ?>"><br></p>
      <p><label>　商品画像　</label><input type="file" name="Image"><br>
      <p><label>　　紹介文　</label><br>
                　　　　　　<textarea cols="40" rows=5 name="Introduction"><?php if(!empty($_SESSION['Introduction'])){ echo $_SESSION['Introduction'];}?></textarea><br>
      <p><label>　　　価格　</label><input type="number" name="Price" value="<?php if(!empty($_SESSION['Price'])){ echo $_SESSION['Price'];}?>"> 円
             <p>　　　　　　　　<input type="submit"  name="send" value="登録する"></p>
    </form>
  </body>
</html>
