<?php
require "function.php";
session_start();
userValidation($_POST);
$_SESSION = $_POST;
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>会員登録内容確認</title>
  </head>

  <body>
    <h1>　ECサイト</h1>
    <h2>　会員登録確認</h2>
    <p>　会員登録内容を確認してください</p>
    <form action="user_complete.php" method="post">
      <label for="">　　　　　　名前:　</label>
      <?php echo htmlspecialchars($_SESSION["name"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">　　　　　　住所:　</label>
      <?php echo htmlspecialchars($_SESSION["address"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">　メールアドレス:　</label>
      <?php echo htmlspecialchars($_SESSION["email"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">　　　パスワード:　</label>
      <?php echo htmlspecialchars($_SESSION["pass"], ENT_QUOTES, "UTF-8"); ?><br>
      <br>

      　　　　　　<input type="button" onclick="history.back();" value="戻る">
      　<input type="submit" value="送信する">
    </form>
  </body>
</html>
