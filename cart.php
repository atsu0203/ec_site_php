<?php
  require "function.php";
  require "db.php";
  session_start();

  if (empty($_SESSION["user"]["id"])){
    header('location: login.php');
  }

$products = productListDB();

$total =0;
foreach ($_SESSION["cart"] as $productID => $amount){
   $cartProduct = productDB($productID);
   $subtotal = $cartProduct["Price"] * $amount;
   $total = $total + $subtotal;
 }

 if(!empty($_POST["deleteCart"])){
   $delete = productDB($_POST["cart"]);
   unset($_SESSION["cart"][$delete['id']]);
   $total =0;
   foreach ($_SESSION["cart"] as $productID => $amount){
      $cartProduct = productDB($productID);
      $subtotal = $cartProduct["Price"] * $amount;
      $total = $total + $subtotal;
    }
  }

?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>商品一覧</title>
  </head>

  <html>
    <body>
      <h1>　ECサイト</h1>
      <h2>　商品一覧</h2>
      　<?php echo htmlspecialchars($_SESSION["user"]["name"], ENT_QUOTES, "UTF-8"); ?>さん
      <form action='login.php'method="POST" style="display:inline">
      　　<input type="submit"  name="logout" value="ログアウト">
      </form>

    <form action='product_list.php'method="POST" style="display:inline">
  　<input type="submit"  name="listBack" value="商品一覧に戻る">
      </form>
      <br>

      <p>カートに入れた商品</p>
      <?php if(!empty($_SESSION["cart"])){ foreach($_SESSION["cart"] as $product => $amount){?>
      <?php  $cart= productDB($product)?>
      <?php  $subtotal = $cart["Price"] * $amount;?>
        <table border=1 >
          <tr>
            <td style="width:100px" align="center"><?php echo htmlspecialchars($cart["Name"], ENT_QUOTES, "UTF-8"); ?></td>
            <td><img src="<?php echo 'img/' . $cart["Image"];?>" width="200" height="150"></td>
            <td style="width:100px" align="center"><?php echo htmlspecialchars($cart["Introduction"], ENT_QUOTES, "UTF-8"); ?></td>
            <td style="width:100px" align="right"><?php echo $cart["Price"]."円　";?></td>
            <td style="width:100px" align="center"><?php echo $amount."こ";?></td>
            <td style="width:100px" align="right"><?php echo $subtotal."円　";?></td>
            <td style="width:100px" align="center"><form action='cart.php' method="POST">
                <input type="hidden" name="cart" value="<?php echo $cart['id'] ;?>">
                <input type="submit"  name="deleteCart" value="削除">
                </form>
          </tr>
          </table>
        <?php } ?>
        <table border=1 >
          <tr>
            <td style="width:520px" align="right"><?php echo "合計　";?></td>

            <td style="width:310px" align="right"><?php echo $total."円　　　　　　　&nbsp;";?></td>
          </tr>
          </table>
      <p>　　　　　　　　　　　　　　　　　　カートの商品の価格合計は<?php echo $total;?>円です</p>
    <?php }else{ echo ("ありません");  } ?></p>

    <form action='product_list.php'method="POST" style="display:inline">
    　　<input type="submit"  name="listback" value="戻る">

  </form>    <form action='order_comfirm.php'method="POST" style="display:inline">
    　　　　　　<input type="submit"  name="addCart" value="購入手続きに進む">
    </form>

    </form>
    </body>
  </html>
