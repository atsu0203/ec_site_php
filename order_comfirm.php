<?php
  require "function.php";
  require "db.php";
  session_start();
  if (empty($_SESSION["user"]["id"])){
    header('location: login.php');
  }

$products = productListDB();

$total =0;
foreach ($_SESSION["cart"] as $productID => $amount){
   $cartProduct = productDB($productID);
   $subtotal = $cartProduct["Price"] * $amount;
   $total = $total + $subtotal;
 }
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>注文確認</title>
  </head>

  <html>
    <body>
      <h1>　ECサイト</h1>
      <h2>　注文確認</h2>
      　<?php echo htmlspecialchars($_SESSION["user"]["name"], ENT_QUOTES, "UTF-8"); ?>さん
      <form action='login.php'method="POST" style="display:inline">
      　　<input type="submit"  name="logout" value="ログアウト">
      </form>

    <form action='product_list.php'method="POST" style="display:inline">
  　<input type="submit"  name="listBack" value="商品一覧に戻る">
      </form>
      <br>

      <form action='order_complete.php'method="POST" style="display:inline">
        <h3>　注文する商品</h3>
        <?php if(!empty($_SESSION["cart"])){ foreach($_SESSION["cart"] as $product => $amount){?>
        <?php  $cart= productDB($product)?>
        <?php  $subtotal = $cart["Price"] * $amount;?>
          <table border=1 >
            <tr>
              <td style="width:100px" align="center"><?php echo htmlspecialchars($cart["Name"], ENT_QUOTES, "UTF-8"); ?></td>
              <td><img src="<?php echo 'img/' . $cart["Image"];?>" width="200" height="150"></td>
              <td style="width:100px" align="center"><?php echo htmlspecialchars($cart["Introduction"], ENT_QUOTES, "UTF-8"); ?></td>
              <td style="width:100px" align="right"><?php echo $cart["Price"]."円　";?></td>
              <td style="width:100px" align="center"><?php echo $amount."こ";?></td>
              <td style="width:100px" align="right"><?php echo $subtotal."円　";?></td>

            </tr>
            </table>
          <?php } ?>
          <table border=1 >
            <tr>
              <td style="width:520px" align="right"><?php echo "合計　";?></td>
              <td style="width:205px" align="right"><?php echo $total."円　";?></td>
            </tr>
            </table>
        <p>　　　　　　　　　　　　　　　　　　　　カートの商品の価格合計は<?php echo $total;?>円です</p>
      <?php }else{ echo ("ありません");  } ?></p>
      <span class="error"><?php if(!empty($_SESSION["address"]["Err"])){ echo htmlspecialchars($_SESSION["address"]["Err"], ENT_QUOTES, "UTF-8"); }?></span>
      <p>　配送先住所</p>
      　　　<input type="radio" name="address" value="<?php echo htmlspecialchars($_SESSION["user"]["address"], ENT_QUOTES, "UTF-8"); ?>" checked="checked"><?php echo htmlspecialchars($_SESSION["user"]["address"], ENT_QUOTES, "UTF-8"); ?>（登録済み住所）<br>
      　　　<input type="radio" name="address" value=""><input type="text" name="changeAddress" value="">(登録済み住所とは違う住所に配送する)<br>
      <p>　支払情報</p>
      　　　<input type="radio" name="payment" value="銀行振込" checked="checked">銀行振込<br>
      　　　<input type="radio" name="payment" value="クレジットカード">クレジットカード<br>
      　　　<input type="radio" name="payment" value="代金引換">代金引換<br>
      　　　<input type="radio" name="payment" value="コンビニ払い">コンビニ払い<br>
      <br>
      　注文を確定しますか<br>
        <input type="hidden" name="price" value="<?php echo $_SESSION['cart'] ;?>">
        <input type="hidden" name="amount" value="<?php echo $_SESSION['cart'] ;?>">
        <input type="hidden" name="total" value="<?php echo $total;?>">
      　<input type="submit"  name="order" value="確定する">
      </form>
      <form action='cart.php'method="POST" style="display:inline">
      　　<input type="submit"  name="listback" value="戻る">
      </form>
    </body>
  </html>
