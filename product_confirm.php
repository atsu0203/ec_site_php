<?php
require "function.php";
session_start();
if(isset($_POST["send"])) {
  if(!empty($_FILES['Image']['tmp_name'])){
    $tempfile = $_FILES['Image']['tmp_name'];
    //アップロード画像の移動先
    $filemove = '/Applications/XAMPP/htdocs/ec_site_php/img/' . $_FILES['Image']['name'];
    //move_uploaded_file関数を使って、アップロードした画像を指定した場所に移動させる
    move_uploaded_file($tempfile , $filemove);
    }
  }
  $_POST["Image"] = $_FILES['Image']['name'];
  productValidation($_POST);
  $_SESSION =$_POST
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>商品登録内容確認</title>
  </head>

  <body>
    <h1>　ECサイト</h1>
    <h2>　商品登録確認</h2>
    <p>　商品登録内容を確認してください</p>
    <form action="product_complete.php" method="post">
      <label for="">　　商品名:　</label>
      <?php echo htmlspecialchars($_SESSION["Name"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">　商品画像:　</label><br>
      　　　　　　<img src="<?php echo 'img/' . $_FILES["Image"]["name"];?>" width="200" height="150"><br>
      <label for="">　　紹介文:　</label>
      <?php echo htmlspecialchars($_SESSION["Introduction"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">　　　価格:　</label>
      <?php echo htmlspecialchars($_SESSION["Price"], ENT_QUOTES, "UTF-8")."円"; ?><br>
      <br>
      　　　　　　<input type="button" onclick="history.back();" value="戻る">
      　<input type="submit" value="送信する">
    </form>
  </body>
</html>
