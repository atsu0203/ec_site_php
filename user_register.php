<?php
session_start();
?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>会員登録</title>
  </head>

  <body>
    <h1>　ECサイト</h1>
    <h2>　会員登録</h2>
    <span class="error"><?php if(isset($_SESSION["nameErr"])){echo $_SESSION["nameErr"]; echo"<br>";} ?>
                        <?php if(isset($_SESSION["addressErr"])){echo $_SESSION["addressErr"]; echo"<br>";} ?>
                        <?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"]; echo"<br>";} ?>
                        <?php if(isset($_SESSION["passwordErr"])){echo $_SESSION["passwordErr"]; echo"<br>";} ?></span>
      <form action='user_confirm.php' method='POST'>
      <p>　名前・住所・メールアドレス・パスワードを入力してください</p>
      <p><label>　　　　　名前　</label><input type="text" name="name"value="<?php if(!empty($_SESSION['name'])){ echo $_SESSION['name']; } ?>"><br></p>
      <p><label>　　　　　住所　</label><input type="text" name="address"value="<?php if(!empty($_SESSION['address'])){ echo $_SESSION['address']; } ?>"><br></p>
      <p><label>メールアドレス　</label><input type="text" name="email" value="<?php if(!empty($_SESSION['email'])){ echo $_SESSION['email'];}?>"><br></p>
      <p><label>　　パスワード　</label><input type="text" name="pass" value="<?php if(!empty($_SESSION['pass'])){ echo $_SESSION['pass'];}?>">
      <p>　　　<a href="login.php"><button type="button">ログインページへ</button></a>　<input type="submit"  name="send" value="登録する"></p>
    </form>
  </body>
</html>
