<?php
// 会員登録バリデーション
function userValidation($data) {
  if(empty($data["name"])) {
    $_POST["nameErr"] = "名前が入力されていません";
    }
  elseif(mb_strlen($data["name"]) > 20){
    $_POST["nameErr"] ="名前の入力上限は20文字です";
    }

  if (empty($data["address"])) {
    $_POST["addressErr"] = "住所が入力されていません";
    }
  if (empty($data["email"])) {
    $_POST["emailErr"] = "メールアドレスが入力されていません";
    }
  if (empty($data["pass"])) {
    $_POST["passwordErr"] = "パスワードが入力されていません";
    }
  if(empty($data["name"])||empty($data["address"])||empty($data["email"])||empty($data["pass"]) ){
    header('location: user_register.php');
    }
}

// ログインバリデーション
function loginValidation($data) {
  if (empty($data["email"])) {
    $_POST["emailErr"] = "メールアドレスが入力されていません";
    }
  if (empty($data["pass"])) {
    $_POST["passwordErr"] = "パスワードが入力されていません";
    }
  if(empty($data["email"])||empty($data["pass"]) ){
    header('location: login.php');
  }
}


// 商品登録バリデーション
function productValidation($data) {
// var_dump($data);exit;
  if(empty($data["Name"])) {
    $_POST["ProductErr"]["Name"] = "商品名が入力されていません";
    }
  elseif(mb_strlen($data["Name"]) > 20){
    $_POST["ProductErr"]["Name"] ="商品名の入力上限は20文字です";
    }
  if (empty($data["Image"])) {
    $_POST["ProductErr"]["Image"] = "商品画像が選択されていません";
    }
  if (empty($data["Introduction"])) {
    $_POST["ProductErr"]["Introduction"] = "紹介文が入力されていません";
    }
  if (empty($data["Price"])) {
    $_POST["ProductErr"]["Price"] = "価格が入力されていません";

  }
  if(empty($data["Name"])|empty($data["Image"])||empty($data["Introduction"])||empty($data["Price"]) ){
    header('location: product_register.php');
  }
}

//住所変更バリテーション
function addressValidation($data) {
  if(!empty($data["address"])){
    $_SESSION["address"] = $data["address"];
  }elseif(!empty($data["changeAddress"])){
    $_SESSION["address"] = $data["changeAddress"];
  }else{
    $_SESSION["address"]["Err"] = "配送先住所の入力がありません";
    header('location: order_comfirm.php');
    return $_SESSION["address"]["Err"];
  }
}
?>
