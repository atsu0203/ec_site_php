<?php
//会員登録
function userRegisterDB(){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアドステートメントを発行したとき、エミュレーションオフの場合はエラーを起こす
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが起こった際に例外をスローにする
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//固定値で配置した値の部分をプレイスホルダーにする
$sql = "INSERT INTO users(id, name, address, email, password)VALUES(NULL,:name, :address, :email, :pass)";
$stmt = $db->prepare($sql);
//bind機構を使用して:userName 部分に変数$userNameに代入されている値を当てはめる
$name = $_SESSION["name"];
$address = $_SESSION["address"];
$email = $_SESSION["email"];
$pass = password_hash($_SESSION["pass"],PASSWORD_BCRYPT);
$stmt->bindParam(':name', $name, PDO::PARAM_STR);
$stmt->bindParam(':address', $address, PDO::PARAM_STR);
$stmt->bindParam(':email', $email, PDO::PARAM_STR);
$stmt->bindParam(':pass', $pass, PDO::PARAM_STR);
$stmt-> execute();
}

//会員ログイン
function userLoginDB($data){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
try{
  $db = new PDO($dsn, $user, $password);
  }
  catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
  }
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "SELECT * FROM users WHERE email = :email";
$stmt = $db->prepare($sql);
$email = $data["email"];
$stmt->bindParam(':email', $email);
$stmt->execute();
$user = $stmt->fetch(PDO::FETCH_ASSOC);
  if(empty($user)){
    header('location: login.php');
    $_POST["emailErr"] = "登録されたメールアドレスはありません";
  }elseif(password_verify($data['pass'], $user['password'])){
   return $user;
  }else{
    header('location: login.php');
    $_POST["mismatchErr"] = "パスワードかメールアドレスに間違いがあります";
  }
}

//会員情報取得
function userDB($userID){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "SELECT * FROM users WHERE id = :id";
$stmt = $db->prepare($sql);
$id = $userID;
$stmt->bindParam(':id', $id);
$stmt->execute();
$user = $stmt->fetch(PDO::FETCH_ASSOC);
return $user;
}



//商品一覧
function productListDB(){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//固定値で配置した値の部分をプレイスホルダーにする
$sql = "SELECT * FROM products";
$stmt = $db->prepare($sql);
$stmt-> execute();
$products = $stmt->fetchAll(PDO::FETCH_ASSOC);
return $products;
}

//商品詳細
function productDB($data){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// DELETE文を変数に格納
$sql = "SELECT * FROM products WHERE id = :id";
$stmt = $db->prepare($sql);
$id = $data;
$stmt->bindParam(':id', $id);
$stmt ->execute();
$product = $stmt->fetch(PDO::FETCH_ASSOC);
return $product;
}


//カート詳細
function cartDB($data          ){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// DELETE文を変数に格納
$sql = "SELECT * FROM products WHERE id = :id";
$stmt = $db->prepare($sql);
$id = $data;
$stmt->bindParam(':id', $id);
$stmt ->execute();
$product = $stmt->fetch(PDO::FETCH_ASSOC);
$sql = "SELECT * FROM products WHERE id = :id";
$stmt = $db->prepare($sql);
$id = $data;
$stmt->bindParam(':id', $id);
$stmt ->execute();
$product = $stmt->fetch(PDO::FETCH_ASSOC);
return array($product,$price);
}


//商品登録
function productRegisterDB(){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアドステートメントを発行したとき、エミュレーションオフの場合はエラーを起こす
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが起こった際に例外をスローにする
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//固定値で配置した値の部分をプレイスホルダーにする
$sql = "INSERT INTO products(id, Name, Image, Introduction, Price)VALUES(NULL,:name, :image, :introduction, :price)";
$stmt = $db->prepare($sql);
//bind機構を使用して:userName 部分に変数$userNameに代入されている値を当てはめる
$name = $_SESSION["Name"];
$image = $_SESSION["Image"];
$introduction = $_SESSION["Introduction"];
$price = $_SESSION["Price"];
$stmt->bindParam(':name', $name, PDO::PARAM_STR);
$stmt->bindParam(':image', $image, PDO::PARAM_STR);
$stmt->bindParam(':introduction', $introduction, PDO::PARAM_STR);
$stmt->bindParam(':price', $price, PDO::PARAM_STR);
$stmt-> execute();
}







//注文データベース登録
function orderDB(){
$dsn = "mysql:dbname=ec_site;host=localhost;charset=utf8";
$user = "root";
$password = "";
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //トランザクション処理を開始
  $db->beginTransaction();
  try {

    $sql = "SELECT max(id) FROM orders";
    $stmt = $db->prepare($sql);
    $stmt-> execute();
    $order = $stmt->fetch(PDO::FETCH_ASSOC);
    $orderId = $order["max(id)"];
    if(empty($orderId)){
      (int)$orderId = 1;
    }else{
      (int)$orderId = (int)$orderId + 1;
    }
     //orders登録
     $sql = "INSERT INTO orders(id, user_id, address, payment, total)VALUES(NULL, :user_id, :address, :payment, :total)";
     $stmt = $db->prepare($sql);

     $userId = $_SESSION["user"]["id"];
     $address = $_SESSION["address"];
     $payment = $_SESSION["payment"];
     $total = $_SESSION["total"];
     $stmt->bindParam(':user_id', $userId, PDO::PARAM_STR);
     $stmt->bindParam(':address', $address, PDO::PARAM_STR);
     $stmt->bindParam(':payment', $payment, PDO::PARAM_STR);
     $stmt->bindParam(':total', $total, PDO::PARAM_STR);
     $stmt-> execute();

     //order登録
     foreach($_SESSION["cart"] as $product => $amount){
       $cart= productDB($product);
       $sql = "INSERT INTO order_detail(id, order_id, product_name, price, amount)VALUES(NULL,:order_id, :product_name, :price, :amount)";
       $stmt = $db->prepare($sql);

       $productName = $cart["Name"];
       $price = $cart["Price"];
       $amount = $amount;
       $stmt->bindParam(':order_id', $orderId, PDO::PARAM_STR);
       $stmt->bindParam(':product_name', $productName, PDO::PARAM_STR);
       $stmt->bindParam(':price', $price, PDO::PARAM_STR);
       $stmt->bindParam(':amount', $amount, PDO::PARAM_STR);
       $stmt-> execute();
       }
     // コミット
     $db->commit();

   }catch(PDOException $e){
     //ロールバック
     $db->rollback();
     // 外側のTryブロックに対してスロー
     throw $e;
   }
 }
