<?php
require "function.php";
require "db.php";
session_start();
if (!empty($_POST["login"])){
  loginValidation($_POST);
  $_POST["user"] = userLoginDB($_POST);
  $_SESSION = $_POST;
}


if (empty($_SESSION["user"]["id"])){
  header('location: login.php');
}

$products = productListDB();

if(!empty($_POST["listBackComplete"])){
  $_SESSION["cart"] =array();
  $total =0;
}


if(!isset($_SESSION["cart"])){
  $_SESSION["cart"] =array();
  $total =0;
}else{

  if(!empty($_POST["addCart"])){
  $add = productDB($_POST["cart"]);

  if(empty($_SESSION["cart"][$add['id']])){
    $_SESSION["cart"][$add['id']]= 1;
  }else{
    $_SESSION["cart"][$add['id']] =  $_SESSION["cart"][$add['id']] + 1;
    }
  }

$total =0;
foreach ($_SESSION["cart"] as $productID => $amount){
  $cartProduct = productDB($productID);
  $subtotal = $cartProduct["Price"] * $amount;
  $total = $total + $subtotal;
  }
}

?>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>商品一覧</title>
  </head>

  <html>
    <body>
      <h1>　ECサイト</h1>
      <h2>　商品一覧</h2>
      　<?php echo htmlspecialchars($_SESSION["user"]["name"], ENT_QUOTES, "UTF-8"); ?>さん
      <form action='login.php'method="POST" style="display:inline">
      　　<input type="submit"  name="logout" value="ログアウト">
      </form>


      <form action='cart.php'method="POST" style="display:inline">
        <input type="hidden" name="cart" value="<?php echo $_SESSION['cart'] ;?>">
    　　<input type="submit"  name="cart" value="カートの中身を見る">
      </form>
      <br>

      <p>カートの商品の価格合計は<?php echo $total;?>円です<p>

      <?php if(!empty($add)){ ?>
      <table border=1>
        <tr>
          <td style="width:100px"><?php echo htmlspecialchars($add["Name"], ENT_QUOTES, "UTF-8"); ?></td>
          <td><img src="<?php echo 'img/' . $add["Image"];?>" width="200" height="150"></td>
          <td style="width:100px"><?php echo htmlspecialchars($add["Introduction"], ENT_QUOTES, "UTF-8"); ?></br>
          <td style="width:100px" align="center"><?php echo $add["Price"]."円";?></td>
          <td style="width:100px" align="center"><?php echo ($_SESSION["cart"][$add['id']]."つ目");?></td>
      </table>
      <p>上記の商品をカートに追加しました</p>
    <?php } ?>

      <br>
      <br>
      　商品を選んでください<br>
      <br>
      <table border=1>
        <tr>
          <th>商品名</th>
          <th>商品画像</th>
          <th>紹介文</th>
          <th>価格</th>
          <th>詳細</th>
        <tr>
            <?php foreach ($products as $product) {?>
            <td style="width:100px" align="center"><?php echo $product["Name"];?></td>
            <td><img src="<?php echo 'img/' . $product["Image"];?>" width="200" height="150"></td>
            <td style="width:100px" align="center"><?php echo $product["Introduction"];?></td>
            <td style="width:100px" align="right"><?php echo $product["Price"]."円　";?></td>
            <td style="width:100px" align="center"><form action='product_detail.php' method="POST">
                <input type="hidden" name="name" value="<?php echo $_SESSION['name'] ;?>">
                <input type="hidden" name="email" value="<?php echo $_SESSION['email'] ;?>">
                <input type="hidden" name="id" value="<?php echo $product['id'] ;?>">
                <input type="submit"  name="detail" value="詳細">
                </form>
        <tr>
        <?php }?>
      </table>
    </body>
  </html>
